<?php

$mongoClient = new MongoClient("mongodb://localhost:27017", array( "connect" => true));
$collection = $mongoClient->selectCollection("phpmongodbkt","activities");
date_default_timezone_set("UTC");

echo "#### Insert 4 New Activities ####" . "</br>";

/* $date_time = DateTime::createFromFormat('Y-m-d H:i:s', "2011-08-21 14:12:55");
$activity = array(
		"activity_id" => "php_act_001",
		"actor" => array(
				"actor_id" => "php001",
				"displayName" => "User"
		),
		"points" => 10,
		"published" => new MongoDate($date_time->getTimestamp())
);
$collection->insert($activity,array("w"=>1));

$date_time = DateTime::createFromFormat('Y-m-d H:i:s', "2012-07-12 01:19:23");
$activity = array(
		"activity_id" => "php_act_002",
		"actor" => array(
				"actor_id" => "php002",
				"displayName" => "Actor"
		),
		"points" => 12,
		"published" => new MongoDate($date_time->getTimestamp())
);
$collection->insert($activity,array("w"=>1));

$date_time = DateTime::createFromFormat('Y-m-d H:i:s', "2014-12-12 17:12:18");
$activity = array(
		"activity_id" => "php_act_003",
		"actor" => array(
				"actor_id" => "php003",
				"displayName" => "Actor"
		),
		"points" => 15,
		"published" => new MongoDate($date_time->getTimestamp())
);
$collection->insert($activity,array("w"=>1));

$date_time = DateTime::createFromFormat('Y-m-d H:i:s', "2015-01-15 16:18:19");
$activity = array(
		"_id" => new MongoId("55116d0627f52e8d0a5d1c72"),
		"activity_id" => "php_act_004",
		"actor" => array(
				"actor_id" => "php004",
				"displayName" => "Participant"
		),
		"points" => 11,
		"published" => new MongoDate($date_time->getTimestamp())
);
$collection->insert($activity,array("w"=>1)); */

echo "#### Find All Activities ####" . "</br>";
$cursor = $collection->find();
while($cursor->hasNext()){
	$activity = $cursor->getNext();
	echo json_encode($activity) . "</br>";
}

echo "#### Find Activity by Id 55116d0627f52e8d0a5d1c72 ####" . "</br>";
$activity = $collection->findOne(array("_id" => new MongoId("55116d0627f52e8d0a5d1c72")));
echo json_encode($activity) . "</br>";

echo "#### Find one Activity with actor name \"User\" ####" . "</br>";
$cursor = $collection->find(array("actor.displayName" => "User"));
while($cursor->hasNext()){
	$activity = $cursor->getNext();
	echo json_encode($activity) . "</br>";
	echo json_encode(date('Y-m-d H:i:s', $activity["published"]->sec)) . "</br>";
}

echo "#### Sort by Date DESC and then Limit to 2 ####" . "</br>";
$cursor = $collection->find()->sort(array("published" => -1))->limit(2);
while($cursor->hasNext()){
	$activity = $cursor->getNext();
	echo json_encode($activity) . "</br>";
	echo json_encode(date('Y-m-d H:i:s', $activity["published"]->sec)) . "</br>";
}

echo "#### Find Activity with Max and Min points WITHOUT Aggregation ####" . "</br>";
$cursor = $collection->find()->sort(array("points" => -1))->limit(1);
while($cursor->hasNext()){
	$activity = $cursor->getNext();
	echo json_encode($activity) . "</br>";
}
$cursor = $collection->find()->sort(array("points" => 1))->limit(1);
while($cursor->hasNext()){
	$activity = $cursor->getNext();
	echo json_encode($activity) . "</br>";
}

echo "#### Find Status/Size of activities collection ####" . "</br>";
$count = $collection->count();
echo "Count of all activities: " . $count . "</br>";
$count = $collection->count(array("actor.displayName" => "Actor"));
echo "Count of all activities with actor Name - Actor: " . $count . "</br>";
?>