package com.mongodbkt;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

public class ActivityDAOImpl {

	public Activity findById(final MongoTemplate mongoTemplate,
			final String collectionName, final String id) {
		Activity activity = mongoTemplate.findById(id, Activity.class,
				collectionName);
		return activity;
	}
	
	public Activity findOne(MongoTemplate mongoTemplate, String collectionName, String actorName) {
		Query query = new Query(Criteria.where("actor.displayName").is(actorName));
		Activity activity = mongoTemplate.findOne(query, Activity.class, collectionName);
		return activity;
	}

	public void insert(final MongoTemplate mongoTemplate,
			final String collectionName, final String activityId, final int points,
			final String actorId, final String actorName, final Date publishDate) {

		Actor actor = new Actor(actorId, actorName);
		Activity activity = new Activity(activityId, points, actor, publishDate);
		
		mongoTemplate.insert(activity, collectionName);
	}
	
	public void insert(final MongoTemplate mongoTemplate, final String collectionName,
			final String objectId, final String activityId, final int points, final String actorId,
			final String actorName, final Date publishDate) {
		Actor actor = new Actor(actorId, actorName);
		Activity activity = new Activity(objectId, activityId, points, actor, publishDate);
		
		mongoTemplate.insert(activity, collectionName);
	}
	
	public void insert(final MongoOperations mongoOperation,
			final String collectionName, final String activityId, final Integer points,
			final String actorId, final String actorName, final Date publishDate) {

		Actor actor = new Actor(actorId, actorName);
		Activity activity = new Activity(activityId, points, actor, publishDate);
		
		mongoOperation.save(activity, collectionName);
	}
	
	public List<Activity> findAll(final MongoTemplate mongoTemplate, final String collectionName) {
		
		return mongoTemplate.findAll(Activity.class, collectionName);
	}

	public List<Activity> sortByPublishDate(final MongoTemplate mongoTemplate, final String collectionName) {
		Query query = new Query();
		query.with(new Sort(Sort.Direction.DESC, "published"));
		query.limit(2);
		return mongoTemplate.find(query, Activity.class, collectionName);
	}

	public Activity getActivityWithMaxPoints(MongoTemplate mongoTemplate,
			String collectionName) {
		Query query = new Query();
		query.with(new Sort(Sort.Direction.DESC, "points"));
		query.limit(1);
		return mongoTemplate.findOne(query, Activity.class, collectionName);
	}
	
	public Activity getActivityWithMinPoints(MongoTemplate mongoTemplate,
			String collectionName) {
		Query query = new Query();
		query.with(new Sort(Sort.Direction.ASC, "points"));
		query.limit(1);
		return mongoTemplate.findOne(query, Activity.class, collectionName);
	}
	
	public List<Activity> findAllActivitiesWithActorName(MongoTemplate mongoTemplate, String collectionName, String actorName) {
		Query query = new Query(Criteria.where("actor.displayName").is(actorName));
		List<Activity> activities = mongoTemplate.find(query, Activity.class, collectionName);
		return activities;
	}
}
